<?php
// PHP Image processing tool
// Settings
// Copyright 2018 - Stef van Dijk for Willems Classics B.V.

// Get Vars
// Image: An image stored in the sourcePath folder
// Auto: Requires a key, CRON-job like autodiscovery of new images
// Forced: Forces recreation
// Verbose: Log everything, maybe only with a key?

// Add some safety
define('CRONKEY', 'imageplease');

// This is where we will find our original images
define('SOURCEPATH', '../images/large');
define('ALT_SOURCEPATH', '../images');
define('ALT_ALT_SOURCEPATH', '../images/categories');

// This is where we will store our images
define('DESTPATH', './images');

// Define sizes required
$sizes = array(
	'lazy' => array(20, 20),
	'small' => array(200, 200),
	'cat' => array(400, 300),
	'medium' => array(400, 400),
	'large' => array(800, 800)
);
define('IMAGESIZES', json_encode($sizes));
