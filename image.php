<?php
// PHP Image processing tool
// API for images
// Copyright 2018 - Stef van Dijk for Willems Classics B.V.

include_once('imagesettings.php');
include_once('imagefunctions.php');

// Get Vars
// Image: The image filename
// Size: Width/Height
// Safe: If not created yet, create the image on the fly (in a size closest to the sizes array)
// Forced: Recreate the image on the exact size required

echo Image($_GET['image']);

?>