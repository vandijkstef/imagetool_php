<?php
// PHP Image processing tool
// Functions
// Copyright 2018 - Stef van Dijk for Willems Classics B.V.

require 'imagesettings.php';

// Functions

function Image($src) {
    // TODO: Add size
    return '<img src="sources/' . $src . '">';
}

function GetImage($src, $sizes, $alt, $domain = '/') {
    $src = str_replace('.png', '.jpg', $src);
    if ($alt == 'url') {
        return GetPath($src, $sizes, $domain);
    } else {
        $img = '<img
		src="' . GetPath($src, $sizes, $domain) . '"
		alt="' . strip_tags($alt) . '"
		>';

        return preg_replace("/\r|\n/", "", $img);
    }
}

function GetLazyImage($src, $sizes, $alt, $domain = '/') {
    $src = str_replace('.png', '.jpg', $src);
    if ($alt == 'url') {
        return GetPath($src, $sizes, $domain);
    } else {
        $img = '<img
		src="' . GetPath($src, ParseSize('lazy'), $domain) . '"
		alt="' . strip_tags($alt) . '"
		class="lazy"
		width="' . $sizes[0] . '"
		height="' . $sizes[1] . '"
		data-src="' . GetPath($src, $sizes, $domain) . '"
		>';

        return preg_replace("/\r|\n/", "", $img);
    }
}

function GetSourceSet($src, $reqSize, $alt, $domain = '/', $sizes = null) {
    $img = '<img
	src="' . GetPath($src, $sizes, $domain) . '"
	alt="' . strip_tags($alt) . '"
	style="max-width: 100%; max-height: ' . $sizes[1] . 'px;" ';
    $allSizes = json_decode(IMAGESIZES);
    $img .= 'srcset="';
    foreach ($allSizes as $size) {
        $img .= GetPath($src, $size, $domain);
        $img .= ' ' . $size[0] . 'w,';
    }
    $img .= '" ';
    if ($sizes != null) {
        $img .= 'sizes="' . $sizes . '" ';
    }
    $img .= '>';

    return preg_replace("/\t/", " ", preg_replace("/\r|\n/", "", $img));
}

function ImageExists($src, $sizes) {
    $path = realpath(DESTPATH . '/' . $sizes[0] . 'x' . $sizes[1]) . '/' . $src;
    $path = str_replace('.png', '.jpg', $path);

    return file_exists($path);
}

function CreateImage($src, $sizes) {
    // Get/Test source file
    $src = urldecode($src);
    $srcPath = '';
    if (file_exists(SOURCEPATH . '/' . $src)) {
        $srcPath = SOURCEPATH . '/' . $src;
    } else if (file_exists(ALT_SOURCEPATH . '/' . $src)) {
        $srcPath = ALT_SOURCEPATH . '/' . $src;
    } else if (file_exists(ALT_ALT_SOURCEPATH . '/' . $src)) {
        $srcPath = ALT_ALT_SOURCEPATH . '/' . $src;
    } else {
        msg('Could not find source');

        return;
    }
    $image = new Imagick($srcPath);

    // Process the source
    $image->setImageCompression(Imagick::COMPRESSION_JPEG);
    if ($sizes[0] == ParseSize('lazy')[0]) {
        $image->setImageCompressionQuality(20);
    } else {
        $image->setImageCompressionQuality(70);
    }
    $image->resizeImage($sizes[0], $sizes[1], Imagick::FILTER_CUBIC, .5, true);

    $destFileName = str_replace('.png', '.jpg', $src);
    $destFileName = explode('/', $destFileName);
    $destFileName = $destFileName[Count($destFileName) - 1];
    $destPath = realpath(DESTPATH . '/' . $sizes[0] . 'x' . $sizes[1]) . '/' . $destFileName;
    $destPath = str_replace('categories/', '', $destPath);
    msg($destPath);
    $image->writeImage($destPath);
}

function GetPath($src, $sizes, $domain = '/') {
    $path = $domain . $sizes[0] . 'x' . $sizes[1] . '/' . $src;
    $path = str_replace('.png', '.jpg', $path);

    return $path;
}

function ParseSize($size) {
    $type = gettype($size);
    if (intval($size) != 0) {
        $type = gettype(intval($size));
    }
    $allSizes = json_decode(IMAGESIZES);
    switch ($type) {
    case 'string':
        $sizes = $allSizes->{$size};
        break;
    case 'integer':
        $sizes = [$size, $size];
        break;
    }
    if (!in_array($sizes, (array) $allSizes)) {
        end($allSizes);
        $sizes = $allSizes->{key($allSizes)};
    }

    return $sizes;
}

// Folders
function SetFolders($sizes) {
    msg('Testing folders...');
    foreach ($sizes as $size) {
        SetFolder($size);
    }
    msg('Done setting folders');
}

function SetFolder($sizes) {
    $path = DESTPATH . '/' . $sizes[0] . 'x' . $sizes[1] . '/';
    if (!file_exists($path)) {
        mkdir($path, 0755);
        msg('Created ' . $path);
    }
}

// Helpers
function msg($message) {
    if ($_GET['verbose'] == true) {
        echo $message . '<br />';
    }
}

?>