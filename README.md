# Imagetool

PHP tool to convert images in multiple (optimised/compressed) sizes. Will export JPG by default, with optional support for webP and low-res SVG.

## Requirements

The project current is build based on `PHP 5.6`, and isn't directly portable to `PHP 7.X`. (Support is on the roadmap) Also, PHP's `imagick` module needs to be installed.

## Usage

-- TO BE COMPLETED --
Settings are in `imagesettings.php`.  
The file `imagetool.php` is the processing API.  
The file `image.php` is the PHP based image retrieval. (Default static serving is recommended)  

## Licence

Though open source, all rights are reserved by Stef van Dijk and Willems Classics B.V.
