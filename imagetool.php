<?php
// PHP Image processing tool
// API for processing
// Copyright 2018 - Stef van Dijk for Willems Classics B.V.

// Get Vars
// Auto: Requires a key, CRON-job like autodiscovery of new images
// Forced: Forces recreation
// Verbose: Log everything

error_reporting(E_ERROR | E_WARNING | E_PARSE);

// get Settings
require 'imagesettings.php';
require 'imagefunctions.php';

// Parse GET variables
if (!$_GET['key']) {
    die('No key');
} else if ($_GET['key'] !== CRONKEY) {
    die('False key');
} else {
    if (!$_GET['image'] && !$_GET['process']) {
        die('Unsure what to do');
    } else {
        if ($_GET['image']) {
            $mode = 'single';
        } else {
            $mode = 'all';
        }
        if ($_GET['forced']) {
            $forced = true;
        } else {
            $forced = false;
        }
    }
}

// Make sure we have the required folders, making them doesn't hurt either way
SetFolders($sizes);

// An array of images to process
$images = array();

if ($mode == 'all') {
    // Gather all images in the sources folder
    // We will check later if it's required to process the image
    $files = array_diff(scandir(SOURCEPATH), array('.', '..'));
    $validExt = ['png', 'jpg', 'jpeg', 'gif'];
    foreach ($files as $file) {
        $ext = explode('.', $file);
        $ext = $ext[count($ext) - 1];
        if (in_array(strtolower($ext), $validExt)) {
            array_push($images, $file);
        }
    }
} else {
    array_push($images, $_GET['image']);
}

// Process the array of images
foreach ($images as $image) {
    foreach ($sizes as $size) {
        if ($forced || !ImageExists($image, $size)) {
            msg('Creating: ' . $image . ' on ' . $size[0] . 'x' . $size[1]);
            CreateImage($image, $size);
        } else {
            msg(ImageExists($image, $size));
            msg('Skipped: ' . $image . ' on ' . $size[0] . 'x' . $size[1]);
        }
    }
}

?>